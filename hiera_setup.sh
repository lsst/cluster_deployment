#!/bin/bash

cp /vagrant/hiera.yaml /etc/puppetlabs/puppet/.

mkdir /etc/puppetlabs/code/environments/production/hieradata/nodes
cp /vagrant/asm01.lsst.ncsa.edu.yaml \
   /etc/puppetlabs/code/environments/production/hieradata/nodes/.
