class firewall_lsst_SERV ( $if_name ) {
    notice( "Doing things with SERV interface $if_name" )
}

class firewall_lsst_MGMT ( $if_name ) {
    notice( "Doing things with MGMT interface $if_name" )
}

class firewall_lsst_EXTL ( $if_name ) {
    notice( "Doing things with EXTL interface $if_name" )
}

class firewall_lsst_VIRT ( $if_name ) {
    notice( "Doing things with VIRT interface $if_name" )
}

class firewall_lsst_LOCL ( $if_name ) {
    notice( "Doing things with LOCL interface $if_name" )
}

class firewall_lsst_UNKN ( $if_name ) {
    notice( "Blocking all access to UNKN interface $if_name" )
}


# Create map of IP => INTERFACE_NAME
$keys = keys( $::facts[networking][interfaces] )
$ip_2_if = reduce( $keys, {} ) |$newhash, $if| {
    $ip = $::facts[networking][interfaces][$if][ip]
    if $ip =~ String[1] {
        $newhash + { $ip => $if }
    } else {
        $newhash
    }
}

# Apply firewalls based on network information
each( $ip_2_if ) |$ipaddr,$intf| {
    case $ipaddr {
        /192.168.18/: { $type = 'SERV' }
         /10.142.18/: { $type = 'MGMT' }
        /141.142.18/: { $type = 'EXTL' }
            /10.0.2/: { $type = 'VIRT' }
           /127.0.0/: { $type = 'LOCL' }
             default: { $type = 'UNKN' }
    }
    class { "firewall_lsst_$type" : if_name => $intf }
}


#$keys = keys( $::facts[networking][interfaces] )
#each( $keys ) | $if | {
#    $ip = $::facts[networking][interfaces][$if][ip]
#    if $ip =~ String[1] {
#        case $ip {
#        /192.168.18/: { $type = 'SERV' }
#         /10.142.18/: { $type = 'MGMT' }
#        /141.142.18/: { $type = 'EXTL' }
#            /10.0.2/: { $type = 'VIRT' }
#           /127.0.0/: { $type = 'LOCL' }
#             default: { $type = 'UNKN' }
#        }
#        class { "firewall_lsst_$type" : if_name => $if }
#    }
#}
