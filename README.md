# Automated Cluster Deployment

Files here (so far) are organized around deploying a cluster admin node that will act as:
* xCAT master
* Puppet master
* Gitlab-CE master

# Testing with Vagrant
A Vagrantfile is included in the repo.  Tested with Vagrant 1.8.1 and VirtualBox on Windows 10.